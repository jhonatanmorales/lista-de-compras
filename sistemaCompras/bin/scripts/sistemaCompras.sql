CREATE TABLE productos.articulos (
	id INT auto_increment NOT NULL primary key,
	nombre VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	precion DECIMAL NOT NULL,
	tipoProducto varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci
COMMENT='BD donde se almacenan losproductos de las compras';

ALTER table productos.articulos
change precion precio DECIMAL NOT NULL

INSERT into productos.articulos (id,nombre,precio,tipoProducto)
values(1,"Arros",2400,"general");

SELECT * FROM productos.articulos 
ORDER BY nombre, tipoProducto ASC;

DELETE FROM productos.articulos
WHERE nombre = 'cosa';

SELECT nombre,precio,tipoProducto FROM productos.articulos
WHERE id = 24;

UPDATE  productos.articulos  
SET nombre = 'yuca', precio = 1000, tipoProducto  = 'Verduras'
WHERE id = 24;