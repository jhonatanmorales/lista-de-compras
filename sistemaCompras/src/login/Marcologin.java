package login;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import constantes.RutaImg;
import modelo.ModeloAccionLista;
import principal.MarcoPrincipal;
/**
 * 
 * @author jhony
 * @version 1.0 26/02/22
 *
 * <h1>Clase MarcoLogin</h1> ventana de inicio donde el susario se loguea
 *
 */
public class Marcologin extends JFrame implements ActionListener, FocusListener{

	private Toolkit pantalla;
	private Dimension tamanoPantalla;
	private int anchoP = 0, altoP = 0;
	private JTextField txtUsuario;
	private JPasswordField txtContra;
	private JButton btnCancelar, btnEntrar;
	private JLabel labImg, labMsg, labFondo;
	private String email = "";
	private boolean valido = false;
	
	private MarcoPrincipal marcoInicio;
	private RutaImg img;
	private ModeloAccionLista modelo;
	
	private ResultSet rs;
	
	private String correo;
	private String clave;
	
	/**
	 * constructor de la clase
	 */
	public Marcologin() {
		setTitle("Login");
		centrarVentana();
		setResizable(false);
		setLayout(null);
		
		setIconImage(new ImageIcon(getClass().getResource(img.IMG_PANEL)).getImage());
		
		marcoInicio = new MarcoPrincipal();
		modelo = new ModeloAccionLista();
		
		txtUsuario = new JTextField("Usuario");
		txtUsuario.setBounds(100, 240, 240, 28);
		txtUsuario.setForeground(Color.BLACK);
		txtUsuario.addFocusListener(this);
		
		txtContra = new JPasswordField();
		txtContra.setForeground(Color.BLACK);
		txtContra.setBounds(100, 270, 240, 28);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(100, 310, 100, 30);
		btnCancelar.setBackground(Color.RED);
		btnCancelar.setForeground(Color.WHITE);
		btnCancelar.addActionListener(this);
		
		btnEntrar = new JButton("Entrar");
		btnEntrar.setBounds(240, 310, 100, 30);
		btnEntrar.setBackground(Color.CYAN.darker().darker());
		btnEntrar.setForeground(Color.WHITE);
		btnEntrar.addActionListener(this);
		
		
		labImg = new JLabel(new ImageIcon(img.IMG_LOGIN_ICONO));
		labImg.setBounds(100, 10, 240, 220);
		
		labMsg = new JLabel("Correo invalido");
		labMsg.setForeground(Color.RED);
		labMsg.setVisible(false);
		labMsg.setBounds(100, 220, 240, 28);
		
		labFondo = new JLabel(new ImageIcon(img.IMG_LOGIN_FONDO));
		labFondo.setBounds(0, 0, 500, 400);
		
		
		
		add(labImg);
		add(labMsg);
		add(txtContra);
		add(txtUsuario);
		add(btnEntrar);
		add(btnCancelar);
		add(labFondo);
	}
	
	/**
	 * metodo encargado de centrar la ventana del login
	 */
	public void centrarVentana() {
		pantalla = Toolkit.getDefaultToolkit();
		
		tamanoPantalla = pantalla.getScreenSize();
		
		anchoP = (int) tamanoPantalla.getWidth();
		altoP = (int) tamanoPantalla.getHeight();
		
		setSize(anchoP/3, altoP/2);
		setLocation(anchoP/3, altoP/3);
	}
	
	
	/**
	 * evento para cerrar la ventana y validar al usuario
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equalsIgnoreCase("cancelar")) {
			
			int opcion = JOptionPane.showConfirmDialog(null,
					"Estas seguro que desea salir?","Salir"
					, JOptionPane.YES_NO_CANCEL_OPTION,
					JOptionPane.QUESTION_MESSAGE);
			
			if(opcion == 0) {
				dispose();
			}
			
		}else {
			rs = modelo.listarUsuarios(txtUsuario.getText());
			
			try {
				while(rs.next()) {
					 correo = rs.getString(1);
					 clave = rs.getString(2);
				}
				
				if(txtUsuario.getText().equals(correo) && 
						txtContra.getText().equals(clave)) {
					
					marcoInicio.setVisible(true);
					
					this.setVisible(false);
					
				}else{
					labMsg.setVisible(true);
					labMsg.setText("Usuario o contraseña invalida");
				}
			} catch (SQLException e1) {
				JOptionPane.showMessageDialog(null, "Error: "+e1.getMessage(),
						"Error",JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	/**
	 * evento para validar el email
	 */
	@Override
	public void focusLost(FocusEvent e) {
		if(e.getSource() == txtUsuario) {
			email = txtUsuario.getText();
			valido = false;
			
			if(email.contains("@") && email.contains(".")) {
				valido = true;
				labMsg.setVisible(false);
				txtUsuario.setForeground(Color.BLACK);
				btnEntrar.setEnabled(true);
			}else {
				labMsg.setVisible(true);
				txtUsuario.setForeground(Color.RED);
				btnEntrar.setEnabled(false);
			}
		}
	}
	
	@Override
	public void focusGained(FocusEvent e) {
		
	}

}
