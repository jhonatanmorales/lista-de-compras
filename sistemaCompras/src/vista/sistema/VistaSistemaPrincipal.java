package vista.sistema;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

import login.Marcologin;
/**
 * 
 * @author jhony
 * @version 1.0 26/02/22
 *
 * <h1>Clase VistaSistemaPrincipal</h1> es la encargada de mostrar toda la aplicasion
 *
 */
public class VistaSistemaPrincipal {

	public Marcologin login;

	public static void main(String[] args) {

		try {
			//cambia la apariencia de la interface
			JFrame.setDefaultLookAndFeelDecorated(true);
			JDialog.setDefaultLookAndFeelDecorated(true);
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
			// UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
			// UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
			// UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
			
			Marcologin login = new Marcologin();
			login.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			login.setVisible(true);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Error: "+e.getMessage());
		}
	}

}
