package principal;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.print.PrinterException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

import modelo.ConeccionBD;
import modelo.ResultSetModeloTabla;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
/**
 * 
 * @author jhony
 * @version 1.0 26/02/22
 *
 * <h1>Clase MarcoTabla</h1> tienen toda la logica de las operaciones
 * para manipular los datos de la tabla exceptuando la funcion de agregar
 *
 */
public class MarcoTabla extends JFrame {

	private JLabel lblTitulo, lblimgFondo, lblImgTabla;
	private JButton btnVolver, btnImprimir, btnEditar, btnEliminar;

	private Statement statement;
	private PreparedStatement statementPreparado;
	private ResultSet rs;
	private ResultSetModeloTabla modeloTabla;
	private JTable tabla;
	private JScrollPane scroll;
	private MarcoPrincipal marcoPrincipal;
	private JFrame marcoEliminar, marcoEditar, marcoBuscar;

	// marco para eliminar
	private JTextField txtEliminar;
	private JButton btnConfirmar;

	// marco para editar
	private JTextField txtNombre, txtPrecio, txtId, txtCantidad;
	private JComboBox<String> listaTipo;
	private String[] lista = { "General", "Verduras", "Aseo", "Frutas" };
	private JButton btnActualizar, btnBuscar;

	/**
	 * medoto constructor que inicializa todos los elementos
	 */
	public MarcoTabla() {
		setTitle("Tabla");
		setExtendedState(MAXIMIZED_BOTH);

		setLayout(null);
		setBackground(new Color(102, 183, 176));
		setIconImage(new ImageIcon(getClass()
				.getResource("/img/tablaIcon.png")).getImage());

		lblTitulo = new JLabel("Lista de productos");
		lblTitulo.setFont(new Font("Liberation Sans", Font.BOLD, 70));
		lblTitulo.setBounds(425, 40, 900, 100);

		lblimgFondo = new JLabel(new ImageIcon("src/img/fondoMarcos.jpg"));
		lblimgFondo.setBounds(0, 0, 1366, 768);

		lblImgTabla = new JLabel(new ImageIcon("src/img/tablaIcon.png"));
		lblImgTabla.setBounds(120, 20, 150, 150);

		btnImprimir = new JButton("Imprimir");
		btnImprimir.setBounds(1020, 660, 320, 60);
		btnImprimir.setFont(new Font("Liberation Sans", Font.BOLD, 24));
		btnImprimir.setBackground(Color.CYAN.darker().darker().darker());
		btnImprimir.setForeground(Color.WHITE);
		//permite imprimir la tabla
		btnImprimir.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					tabla.print();
				} catch (PrinterException e1) {
					JOptionPane.showMessageDialog(null, "Error: "+e1.getMessage());
				}
			}
		});

		btnVolver = new JButton("Volver");
		btnVolver.setBounds(20, 660, 320, 60);
		btnVolver.setFont(new Font("Liberation Sans", Font.BOLD, 24));
		btnVolver.setBackground(Color.RED.darker().darker());
		btnVolver.setForeground(Color.WHITE);
		//se devuelve una vista
		btnVolver.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				marcoPrincipal = new MarcoPrincipal();
				marcoPrincipal.setVisible(true);

				setVisible(false);
			}
		});

		txtNombre = new JTextField("Nombre");
		txtNombre.setFont(new Font("Liberation Sans", 0, 16));
		txtNombre.setBounds(30, 30, 170, 40);

		txtPrecio = new JTextField("Precio");
		txtPrecio.setFont(new Font("Liberation Sans", 0, 16));
		txtPrecio.setBounds(220, 30, 170, 40);

		txtCantidad = new JTextField("Cantidad");
		txtCantidad.setFont(new Font("Liberation Sans", 0, 16));
		txtCantidad.setBounds(410, 30, 170, 40);

		listaTipo = new JComboBox<String>(lista);
		listaTipo.setBounds(610, 30, 170, 40);

		txtId = new JTextField("Id producto");
		txtId.setFont(new Font("Liberation Sans", 0, 16));
		txtId.setBounds(80, 30, 170, 40);

		btnActualizar = new JButton("Actualizar");
		btnActualizar.setBounds(800, 30, 145, 40);
		btnActualizar.setFont(new Font("Liberation Sans", Font.BOLD, 16));
		btnActualizar.setBackground(Color.GREEN.darker().darker());
		btnActualizar.setForeground(Color.WHITE);
		//actualiza el producto y realiza la operacion
		btnActualizar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				operarPrecio();
				
				actualizarProducto(Integer.parseInt(txtId.getText()), 
						txtNombre.getText(),
						Integer.parseInt(txtPrecio.getText()), 
						listaTipo.getSelectedItem().toString(),
						Integer.parseInt(txtCantidad.getText()));
			}
		});
		
		btnBuscar = new JButton("Buscar");
		btnBuscar.setBounds(290, 30, 130, 40);
		btnBuscar.setFont(new Font("Liberation Sans", Font.BOLD, 16));
		btnBuscar.setBackground(Color.GREEN.darker().darker());
		btnBuscar.setForeground(Color.WHITE);
		//busca el producto
		btnBuscar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				buscarProducto(Integer.parseInt(txtId.getText()));
			}
		});

		marcoBuscar = new JFrame("Buscar");
		marcoBuscar.setBounds(450, 40, 500, 130);
		marcoBuscar.setLayout(null);
		marcoBuscar.setResizable(false);
		marcoBuscar.add(txtId);
		marcoBuscar.add(btnBuscar);

		marcoEditar = new JFrame();
		marcoEditar.setTitle("Editar");
		marcoEditar.setLayout(null);
		marcoEditar.setResizable(false);
		marcoEditar.setBounds(200, 40, 1000, 140);
		marcoEditar.add(txtNombre);
		marcoEditar.add(txtPrecio);
		marcoEditar.add(txtCantidad);
		marcoEditar.add(listaTipo);
		marcoEditar.add(btnActualizar);

		btnEditar = new JButton("Editar");
		btnEditar.setBounds(350, 660, 320, 60);
		btnEditar.setFont(new Font("Liberation Sans", Font.BOLD, 24));
		btnEditar.setBackground(Color.GREEN.darker().darker());
		btnEditar.setForeground(Color.WHITE);
		//oculta el marco
		btnEditar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				marcoBuscar.setVisible(true);
			}
		});

		marcoEliminar = new JFrame("Eliminar");
		marcoEliminar.setBounds(450, 40, 500, 130);
		marcoEliminar.setLayout(null);

		txtEliminar = new JTextField("Id producto");
		txtEliminar.setFont(new Font("Liberation Sans", 0, 16));
		txtEliminar.setBounds(80, 30, 170, 40);

		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setBounds(290, 30, 130, 40);
		btnConfirmar.setFont(new Font("Liberation Sans", Font.BOLD, 16));
		btnConfirmar.setBackground(new Color(255, 102, 0));
		btnConfirmar.setForeground(Color.WHITE);
		//boton para eliminar producto
		btnConfirmar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				eliminarProducto(txtEliminar.getText());
			}
		});

		marcoEliminar.add(txtEliminar);
		marcoEliminar.add(btnConfirmar);

		btnEliminar = new JButton("Eliminar");
		btnEliminar.setBounds(690, 660, 320, 60);
		btnEliminar.setFont(new Font("Liberation Sans", Font.BOLD, 24));
		btnEliminar.setBackground(new Color(255, 102, 0));
		btnEliminar.setForeground(Color.WHITE);
		//cierra el marco para eliminar producto 
		btnEliminar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				marcoEliminar.setVisible(true);
				marcoEliminar.setResizable(false);
			}
		});

		tabla = new JTable(listarProductosTabla());

		scroll = new JScrollPane(tabla);
		scroll.setBounds(130, 200, 1130, 400);

		add(scroll);
		add(lblTitulo);
		add(lblImgTabla);
		add(btnImprimir);
		add(btnVolver);
		add(btnEditar);
		add(btnEliminar);
		add(lblimgFondo);
	}

	/**
	 * metodo que lista los productos
	 * 
	 * @return una clase con los metodos para manipular la tabla
	 */
	public ResultSetModeloTabla listarProductosTabla() {
		String sql = "SELECT * FROM productos.articulos \n" + "ORDER BY nombre,"
				+ " tipoProducto ASC";

		try {
			statement = (Statement) ConeccionBD.getConexion().createStatement();
			rs = statement.executeQuery(sql);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Error: "+e.getMessage(),
					"Error",JOptionPane.ERROR_MESSAGE);
			try {
				statement.close();
				rs.close();
			} catch (SQLException e1) {
				JOptionPane.showMessageDialog(null, "Error: "+e1.getMessage(),
						"Error",JOptionPane.ERROR_MESSAGE);
			}
		}

		modeloTabla = new ResultSetModeloTabla(rs);

		return modeloTabla;
	}

	/**
	 * metodo que elimina un producto
	 * 
	 * @param id - necesario para eliminar el producto
	 */
	public void eliminarProducto(String id) {
		String sql = "DELETE FROM productos.articulos\n" + "WHERE id = ?";

		try {
			statementPreparado = (PreparedStatement) ConeccionBD.getConexion()
					.prepareStatement(sql);

			statementPreparado.setString(1, id);

			statementPreparado.execute();

			marcoEliminar.setVisible(false);

			JOptionPane.showMessageDialog(null, "Producto elimino exitosamente"
					, "Aviso", JOptionPane.INFORMATION_MESSAGE);
			tabla.setModel(listarProductosTabla());
		} catch (SQLException e1) {
			JOptionPane.showMessageDialog(null, "Error: "+e1.getMessage(),
					"Error",JOptionPane.ERROR_MESSAGE);
			try {
				statementPreparado.close();
			} catch (SQLException e2) {
				JOptionPane.showMessageDialog(null, "Error: "+e2.getMessage(),
						"Error",JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	/**
	 * metodo para buscar un producto
	 * 
	 * @param id - nesesario para buscar un producto
	 */
	public void buscarProducto(int id) {
		String sql = "SELECT nombre,precio,tipoProducto,cantidad FROM"
				+ " productos.articulos \n" + "WHERE id = " + id;

		try {
			statement = (Statement) ConeccionBD.getConexion().createStatement();
			rs = statement.executeQuery(sql);

			while (rs.next()) {
				txtNombre.setText(rs.getString(1));
				txtPrecio.setText(String.valueOf(rs.getInt(2)));
				listaTipo.setSelectedItem(rs.getString(3));
				txtCantidad.setText(String.valueOf(rs.getInt(4)));
			}

			validate();

			marcoEditar.setVisible(true);
			marcoBuscar.dispose();

		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Error: "+e.getMessage(),
					"Error",JOptionPane.ERROR_MESSAGE);
			try {
				statement.close();
				rs.close();
			} catch (SQLException e1) {
				JOptionPane.showMessageDialog(null, "Error: "+e1.getMessage(),
						"Error",JOptionPane.ERROR_MESSAGE);
			}
		}

	}

	/**
	 * actualiza el producto que se busco anteriormente
	 * 
	 * @param id
	 * @param nombre
	 * @param precio
	 * @param tipo
	 * @param cantidad
	 */
	public void actualizarProducto(int id, String nombre, int precio,
			String tipo, int cantidad) {
		
		String sql = "UPDATE  productos.articulos\n" + "SET nombre=?,"
				+ " precio=?, tipoProducto=?, cantidad=?\n"
				+ "WHERE id=?";

		try {
			statementPreparado = (PreparedStatement) ConeccionBD.getConexion()
					.prepareStatement(sql);

			statementPreparado.setString(1, nombre);
			statementPreparado.setInt(2, precio);
			statementPreparado.setString(3, tipo);
			statementPreparado.setInt(4, cantidad);
			statementPreparado.setInt(5, id);

			statementPreparado.executeUpdate();

			marcoEditar.dispose();

			JOptionPane.showMessageDialog(null, "Producto actualizado exitosamente"
					, "Aviso", JOptionPane.INFORMATION_MESSAGE);
			tabla.setModel(listarProductosTabla());
		} catch (SQLException e1) {
			JOptionPane.showMessageDialog(null, "Error: "+e1.getMessage(),
					"Error",JOptionPane.ERROR_MESSAGE);
			try {
				statementPreparado.close();
			} catch (SQLException e2) {
				JOptionPane.showMessageDialog(null, "Error: "+e2.getMessage(),
						"Error",JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	/**
	 * operacion para calcular el precio de la cantidad de productos
	 */
	public void operarPrecio() {
		int operacion = 0, 
				cantidad = Integer.parseInt(txtCantidad.getText()),
				precio = Integer.parseInt(txtPrecio.getText());
				
				for (int i = 0; i < cantidad; i++) {
					operacion += precio;
				}

				txtPrecio.setText(String.valueOf(operacion));
	}

}



