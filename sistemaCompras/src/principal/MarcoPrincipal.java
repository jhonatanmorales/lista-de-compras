package principal;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
import java.sql.SQLException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import com.mysql.jdbc.PreparedStatement;

import constantes.RutaImg;
import modelo.ConeccionBD;
import modelo.ModeloAccionLista;
/**
 * 
 * @author jhony
 * @version 1.0 26/02/22
 *
 * <h1>Clase MarcoPrincipal</h1> es la vista donde se registran los productos
 *
 */
public class MarcoPrincipal extends JFrame{

	private JLabel labTitulo, labImgLista, lblNombre, lblPrecio,
	lblLista, lblimgFondo, lblCantidad;
	private JTextField txtNombre, txtPrecio, txtCantidad;
	private JComboBox<String> listProducto;
	private String[] listaTipo = {"General", "Verduras", "Aseo", "Frutas"};
	private JButton btnLista, btnSalir, btnAgregar;
	private MarcoTabla marcoTabla;
	
	private ModeloAccionLista modelo;
	private RutaImg img;
	
	/**
	 * constructor de la clase
	 */
	public MarcoPrincipal() {
		setTitle("Principal");
		setExtendedState(MAXIMIZED_BOTH);
		
		setLayout(null);
		setBackground(new Color(102, 183, 176));

		setIconImage(new ImageIcon(getClass()
				.getResource(img.IMG_PANEL)).getImage());
		
		modelo = new ModeloAccionLista();
		
		labImgLista = new JLabel(new ImageIcon(img.IMG_LISTA_ICON));
		labImgLista.setBounds(120, 20, 150, 150);
		
		labTitulo = new JLabel("Crea tus listas");
		labTitulo.setFont(new Font("Liberation Sans", Font.BOLD, 70));
		labTitulo.setBounds(450, 40, 900, 100);
		
		lblNombre = new JLabel("Nombre producto:");
		lblNombre.setFont(new Font("Liberation Sans", Font.BOLD, 16));
		lblNombre.setBounds(480, 210, 150, 40);
		
		lblPrecio = new JLabel("Precio producto:");
		lblPrecio.setFont(new Font("Liberation Sans", Font.BOLD, 16));
		lblPrecio.setBounds(480, 300, 150, 40);
		
		lblCantidad = new JLabel("Cantidad producto:");
		lblCantidad.setFont(new Font("Liberation Sans", Font.BOLD, 16));
		lblCantidad.setBounds(480, 385, 150, 40);
		
		lblLista = new JLabel("Tipo producto:");
		lblLista.setFont(new Font("Liberation Sans", Font.BOLD, 16));
		lblLista.setBounds(480, 465, 150, 40);
		
		listProducto = new JComboBox<String>(listaTipo);
		listProducto.setBounds(480, 500, 170, 50);
		
		txtNombre = new JTextField();
		txtNombre.setFont(new Font("Liberation Sans", 0, 20));
		txtNombre.setBounds(480, 250, 380, 50);
		
		txtPrecio = new JTextField("0");
		txtPrecio.setFont(new Font("Liberation Sans", 0, 20));
		txtPrecio.setBounds(480, 340, 380, 50);
		
		txtCantidad = new JTextField("0");
		txtCantidad.setFont(new Font("Liberation Sans", 0, 20));
		txtCantidad.setBounds(480, 420, 380, 50);
		
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(660, 500, 200, 50);
		btnAgregar.setFont(new Font("Liberation Sans", Font.BOLD, 24));
		btnAgregar.setBackground(Color.GREEN.darker().darker());
		btnAgregar.setForeground(Color.WHITE);
		//valida que los campos no sean nulos y realiza la operacion
		btnAgregar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				operarPrecio();
				
				validarCampos();
			}
		});
		
		
		btnLista = new JButton("Lista");
		btnLista.setBounds(1020, 660, 320, 60);
		btnLista.setFont(new Font("Liberation Sans", Font.BOLD, 24));
		btnLista.setBackground(Color.CYAN.darker().darker().darker());
		btnLista.setForeground(Color.WHITE);
		//muestra la lista
		btnLista.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				marcoTabla = new MarcoTabla();
				marcoTabla.setVisible(true);
				
				setVisible(false);
			}
		});
		
		btnSalir = new JButton("Salir");
		btnSalir.setBounds(20, 660, 320, 60);
		btnSalir.setFont(new Font("Liberation Sans", Font.BOLD, 24));
		btnSalir.setBackground(Color.RED.darker().darker());
		btnSalir.setForeground(Color.WHITE);
		//cierra la aplicasion
		btnSalir.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(e.getActionCommand().equalsIgnoreCase("salir")) {
					
					int opcion = JOptionPane.showConfirmDialog(null,
							"Estas seguro que desea salir?","Salir"
							, JOptionPane.YES_NO_CANCEL_OPTION,
							JOptionPane.QUESTION_MESSAGE);
					
					if(opcion == 0) {
						dispose();
					}
					
				}
			}
		});
		
		lblimgFondo = new JLabel(new ImageIcon(img.IMG_MARCOS_FONDO));
		lblimgFondo.setBounds(0, 0, 1366, 768);

		add(labImgLista);
		add(labTitulo);
		add(lblNombre);
		add(lblPrecio);
		add(lblCantidad);
		add(lblLista);
		add(btnAgregar);
		add(listProducto);
		add(txtNombre);
		add(txtPrecio);
		add(txtCantidad);
		add(btnLista);
		add(btnSalir);
		add(lblimgFondo);
	}
	
	/**
	 * opera el precio segun la cantidad
	 */
	public void operarPrecio() {
		int operacion = 0, 
				cantidad = Integer.parseInt(txtCantidad.getText()),
				precio = Integer.parseInt(txtPrecio.getText());
				
				for (int i = 0; i < cantidad; i++) {
					operacion += precio;
				}

				txtPrecio.setText(String.valueOf(operacion));
	}
	
	/*
	 * limpia los campos
	 */
	public void limpiarCampos() {
		txtNombre.setText("");
		txtPrecio.setText("0");
		txtCantidad.setText("0");
	}
	
	/**
	 * valida los campos y registra el producto
	 */
	public void validarCampos() {
		if(txtNombre.getText().equals("") || txtNombre.getText() == null)
			JOptionPane.showMessageDialog(null,
					"Cuidado, no ingreso ningun nombre",
					"Advertencia",JOptionPane.WARNING_MESSAGE);
		else if(txtPrecio.getText().equals("") || txtPrecio.getText() == null)
			JOptionPane.showMessageDialog(null,
					"Cuidado, no ingreso ningun precio",
					"Advertencia",JOptionPane.WARNING_MESSAGE);
		else if(txtCantidad.getText().equals("") || txtCantidad.getText() == null)
			JOptionPane.showMessageDialog(null,
					"Cuidado, no ingreso ninguna cantidad",
					"Advertencia",JOptionPane.WARNING_MESSAGE);
		
		else {
			modelo.insertarProductos(txtNombre.getText(),
					Double.parseDouble(txtPrecio.getText()), 
					listProducto.getSelectedItem().toString(),
					Integer.parseInt(txtCantidad.getText()));
			
			limpiarCampos();
		}
	}
	
}
