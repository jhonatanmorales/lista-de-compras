package constantes;
/**
 * 
 * @author jhony
 * @version 1.0 26/02/22
 *
 * <h1>Clase RutaImg</h1> tienen todas las imagenes de la aplicasion
 *
 */
public class RutaImg {

	//fondo del login
	public static final String IMG_LOGIN_FONDO = "src/img/fondo.jpg";
	//icono del login
	public static final String IMG_LOGIN_ICONO = "src/img/usuario.png";
	//imagen del panel
	public static final String IMG_PANEL = "/img/tablaIcon.png";
	//imagen de las listas
	public static final String IMG_LISTA_ICON = "src/img/lista.png";
	//fondo de los marcos
	public static final String IMG_MARCOS_FONDO = "src/img/fondoMarcos.jpg";

}
