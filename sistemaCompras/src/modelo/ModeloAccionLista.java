package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import com.mysql.jdbc.PreparedStatement;
/**
 * 
 * @author jhony
 * @version 1.0 26/02/22
 *
 * <h1>Clase ModeloAccionLista</h1> tiene los metodos que interactuan con la BD
 *
 */
public class ModeloAccionLista {
	
	private PreparedStatement sentencia;
	private ResultSet rs;
	
	/**
	 * inserta los productos
	 * 
	 * @param nombre
	 * @param precio
	 * @param tipoProducto
	 * @param cantidad
	 */
	public void insertarProductos(String nombre, double precio,
			String tipoProducto, int cantidad) {
		String sql = "INSERT into productos.articulos "
				+ "(nombre,precio,tipoProducto,cantidad)\n" + 
				"values(?,?,?,?);";
		
		try {
			sentencia = (PreparedStatement) ConeccionBD
					.getConexion().prepareStatement(sql);
			
			sentencia.setString(1, nombre);
			sentencia.setDouble(2, precio);
			sentencia.setString(3, tipoProducto);
			sentencia.setInt(4, cantidad);
			
			sentencia.execute();
			
			JOptionPane.showMessageDialog(null, "Producto agregado exitosamente"
					, "Aviso", JOptionPane.INFORMATION_MESSAGE);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Error: "+e.getMessage(),
					"Error",JOptionPane.ERROR_MESSAGE);
		}
	}
	
	/**
	 * lista los usuarios
	 * 
	 * @param correo
	 * @return rs - un ResulSet que contienen los registros
	 */
	public ResultSet listarUsuarios(String correo) {
		String sql = "SELECT correo, clave FROM productos.usuarios\n" + 
				"WHERE correo = ?";
		
		
		try {
			
			sentencia = (PreparedStatement) ConeccionBD
					.getConexion().prepareStatement(sql);
			
			sentencia.setString(1, correo);
			
			rs = sentencia.executeQuery();
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Error: "+e.getMessage(),
					"Error",JOptionPane.ERROR_MESSAGE);
		}
		
		return rs;
	}
}





