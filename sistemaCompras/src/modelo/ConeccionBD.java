package modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JOptionPane;
/**
 * 
 * @author jhony
 * @version 1.0 26/02/22
 *
 * <h1>Clase ConeccionBD</h1> singleton que crea la conexion con la BD
 *
 */
public class ConeccionBD {

	private static Connection conexion;
	
	public static Connection getConexion() {
		try {
			if(conexion == null) {
				conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/productos",
						 "root", "");
			}
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Error: "+e.getMessage());
			try {
				conexion.close();
			} catch (SQLException e1) {
				JOptionPane.showConfirmDialog(null, "Error: "+e1.getMessage());
			}
		}
		
		return conexion;
	}
}
