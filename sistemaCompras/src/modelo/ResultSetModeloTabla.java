package modelo;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import javax.swing.table.AbstractTableModel;
/**
 * 
 * @author jhony
 * @version 1.0 26/02/22
 *
 * <h1>Clase ResultSetModeloTabla</h1> tiene los metodospara manipular la tabla
 *
 */
public class ResultSetModeloTabla extends AbstractTableModel {

	private ResultSet rsRegistros;
	private ResultSetMetaData resmd;
	
	public ResultSetModeloTabla(ResultSet unResultSet) {
		rsRegistros = unResultSet;
		
		try {
			resmd = rsRegistros.getMetaData();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public int getColumnCount() {
		
		try {
			return resmd.getColumnCount();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public int getRowCount() {
		
		try {
			rsRegistros.last();
			return rsRegistros.getRow();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public Object getValueAt(int arg0, int arg1) {
		try {
			rsRegistros.absolute(arg0 + 1);
			return rsRegistros.getObject(arg1 + 1);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	//muestra el nombre de los campos
	public String getColumnName(int c) {
		try {
			return resmd.getColumnName(c+1);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
